(function (angular) {
  'use strict:';

  var app = angular.module('angular-gulp-dojo');
  app.service('AttrService', function(){
    var attrName;

    this.setAttrName = function(name){
      attrName = name;
    }

    return{
      restrict: 'E',
      scope:{
        'msg': '@'
      },
      templateUrl: 'diretivas/loading/loading.html'
    };
  });
})(angular);
