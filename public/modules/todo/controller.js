(function(angular){
  'use strinct';
  var app = angular.module('angular-gulp-dojo');
  app.controller('TodoController', function($scope, $http) {

    var url = 'https://angular-gulp-dojo-backend.herokuapp.com/api/task/';
    var config = { headers: { 'Content-Type': 'application/json' } };

    loadTasks();
    $scope.loading = false;

  function loadTasks() {
      $http.get(url)
      .success(function(tasks) {
        $scope.tasks = tasks;
        $scope.newTask = null;
        $scope.loading = false;
      });
    }

    function newTask(task) {
      $scope.loading = true;
        $http.post(url, task)
        .success(function() {
          loadTasks();
        });
      }

      $scope.DeleteTask = function delTask(task) {
        $scope.loading = true;
          $http.delete(url + task._id)
          .success(function() {
            loadTasks();
          });
        }

        $scope.UpdateTask = function upTask(task) {
            $scope.newTask = angular.copy(task);

            // $scope.newTask = task;
            // .success(function() {
            //   loadTasks();
            // });
          }
          $scope.UpSertTask = function upSertTask(task) {
            $scope.loading = true;
            if(!task._id) {
                newTask(task);
              }
              else {
                $http.put(url, task)
                .success(function() {
                  loadTasks();
                });
              }
            }
  });
})(angular);
